# README #


### Blink Reaction Coding Challenge ###

* .module and .info files for mysearch module
* 7.x-1.0

### How do I get set up? ###

* Put both files in sites/all/modules/custom/mysearch (or similar, depending on your site structure)
* Enable the module
* Grant the 'Access My Search" permission to the appropriate user

### Notes ###

* Added t() to array keys in mysearch_permission
* Changed Implementation to Implements
* Added wildcard to menu url
* changed 'type' to MENU_CALLBACK so that visiting the url will trigger the function
* added argument to mysearch_searchpage
* sanitized the search term
* used entityFieldQuery to search for the term
* check to make sure there are results, if not, display a message to the user
* counted the results to display to the user
* instead of loading each node, it is queried for the title, this is a bit faster (i used Devel Generate to put in about 500 nodes and it was still very fast)
* used the link function to create the search result url
* fixed typos, added ending </ul> tag, removed debugging code
* I thought about adding the search term to the title in the menu, but I wanted to display the number of results, and I thought that if the search term were long, it would create a cumbersome looking title, so I decided to keep the page title simple and display the term and the number of results under it
* I also queried the body field to return a snippet of text to give the results context. I used LEFT to grab all the text to the left of the term, and then 50 characters to the right. I then trimmed the text to 100 characters from the end, so the search term would be in the middle. Then I replaced the search term in the text with a bold version.